<?php

namespace Model;

use BusinessClass\Question;

/**
 * Décrit les fonctionnalités principales d'une fabrique
 */
abstract class Factory
{
    /**
     * Permet de créer un objet grâce au retour d'une Gateway.
     *
     * @param array $results
     *
     * @return array
     */
    abstract public function create(array $results): array;


    /**
     * Permet de récupérer les objets créés par la fonction create().
     *
     * @param array $results
     * @param string $type
     *
     * @return array
     */
    public static function getBuiltObjects(array $results, string $type): array
    {
        $type = "\\Model\\Factory" . $type;
        return (new $type())->create($results);
    }
}
