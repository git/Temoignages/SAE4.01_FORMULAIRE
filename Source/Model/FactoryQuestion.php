<?php

namespace Model;

use BusinessClass\Question;
use BusinessClass\TextQuestion;

/**
 * Décrit une fabrique de questions
 */
class FactoryQuestion extends Factory
{
    /**
     * Permet de créer une liste de question en fonction du retour d'une gateway
     * passer en paramètre. On prend en compte les différents types de question.
     *
     * @param array $results
     *
     * @return array
     */
    public function create(array $results): array
    {
        $questions = [];
        if ($results[0] != null) {
            for ($i = 0; $i < count($results[0]); $i++) {
                if (strcmp($results[0][$i]->type, "BusinessClass/TextQuestion") == 0) {
                    $questions[] = new TextQuestion($results[0][$i]->id, $results[0][$i]->content);
                } else {
                    $possiblesResponses = $results[1][$i];
                    $content = $results[0][$i]->content;
                    $categories = $results[2][$i];
                    $id = $results[0][$i]->id;
                    $questions[] = new $results[0][$i]->type($possiblesResponses, $content, $categories, $id);
                }
            }
        }

        return $questions;
    }
}
