<?php

namespace Model;

use BusinessClass\Form;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use PDOException;
use Config\Validate;
use Config\Clean;

/**
 * Permet de développer les fonctions appelées par le controllerAdmin pour gérer
 * les actions de l'administrateur
 */
class ModelAdmin
{
    private Client $client;

    public function __construct()
    {
        $this->client = new Client(['headers' => ['Content-Type' => 'application/json'], 'verify' => false]);
    }

    public function goToAdmin(): void
    {
        global $rep, $views;
        try {
            require_once($rep . $views['admin']);
        } catch (PDOException $e) {
            $error = $e->getMessage();
            require_once($rep . $views['form']);
    }
}
    /**
     * Permet de créer et d'ajouter une question et de retourner son ID afin de la
     * reconnaitre facilement dans la suite du code.
     *
     * @return int
     * @throws Exception
     */
    public function addQuestion(): int
    {
        $questionContent = Clean::simpleString($_POST['question']);
        $type = Clean::simpleString($_POST['type']);
        try {
            if (validate::type($type)) {
                $question = new $type(0, $questionContent);
                $res = $this->client->request('GET', 'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/getForm');
                if ($res->getStatusCode() != 200) {
                    throw new Exception('GetForm failed');
                }
                $form = json_decode($res->getBody());
                echo $form;
                if (!empty($form)) {
                    $res = $this->client->request(
                        'POST',
                        'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/addQuestion?'.
                                                'content='.$questionContent.'&'.
                                                'classQuestion='.get_class($question).'&'.
                                                'idForm='.$form[0]['id']
                    );
                    if ($res->getStatusCode() != 200) {
                        throw new Exception('AddQuestion failed');
                    }
                    return json_decode($res->getBody());
                }
            } else {
                throw new Exception('Type de question invalide');
            }
        } catch (GuzzleException | ClientException $g) {
            echo "Error : " . $g->getMessage();
            throw new Exception($g->getMessage(), $g->getCode(), $g);
        }
        return -1;
    }

    /**
     * Permet de supprimer une question du formulaire
     *
     * @return void
     * @throws Exception
     */
    public function deleteQuestion():void
    {
        $idQuestion = Clean::int($_POST["idQuestion"]);
        $type = Clean::simpleString($_POST["type"]);
        try {
            if (!validate::type($type)) {
                throw new Exception('Type de question invalide');
            }
            $res = $this->client->request('DELETE', 'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/deleteQuestion?'.
                                'classQuestion='.$type.'&'.
                                'id='.$idQuestion
            );
            if ($res->getStatusCode() != 200) {
                throw new Exception('DeleteQuestion failed');
            }
        } catch (GuzzleException | ClientException $g) {
            echo "Error : " . $g->getMessage();
            throw new Exception($g->getMessage(), $g->getCode(), $g);
        }
    }

    /**
     * Permet d'ajouter une possibilité de réponse à une question en l'assignant à des catégories.
     *
     * @return void
     * @throws Exception
     */
     public function addResponse(): void
    {
        $idQuestion = Clean::int($_POST['idQuestion']);
        $response = Clean::simpleString($_POST['response']);
        $categories = Clean::simpleStringArray($_POST['categories']);
        if ($categories == null) {
            $categories = [];
        }
        try {
            if (!validate::categories($categories)) {
                throw new Exception('Categories invalides');
            }
            $res = $this->client->request('POST', 'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/insertResponseInQuestion?'.
                                    'response='.$response.'&'.
                                    'categories='.json_encode($categories).'&'.
                                    'idQuestion='.$idQuestion
            );
            if ($res->getStatusCode() != 200) {
                throw new Exception('InsertResponseInQuestion failed');
            }
        } catch (GuzzleException | ClientException $g) {
            echo "Error : " . $g->getMessage();
            throw new Exception($g->getMessage(), $g->getCode(), $g);
        }
    }

    /**
     * Permet de supprimer une possible réponse à une question
     *
     * @return void
     * @throws Exception
     */
    public function deleteResponse(): void
    {
        try {
            $res = $this->client->request('DELETE', 'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/deletePossibleResponse?'.
                                'id='.$_POST["possibleResponse"]
            );
            if ($res->getStatusCode() != 200) {
                throw new Exception('DeletePossibleResponse failed');
            }
        } catch (GuzzleException $g) {
            echo "Error : " . $g->getMessage();
            throw new Exception($g->getMessage(), $g->getCode(), $g);
        }
    }

    /**
     * Permet de créer un nouveau formulaire en précisant son titre et sa description.
     *
     * @return void
     * @throws Exception
     */
    public function createForm(): void
    {
        try {
            $res = $this->client->request('GET', 'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/getForm');
            if ($res->getStatusCode() != 200) {
                throw new Exception('GetForm failed');
            }
            $formulaire = json_decode($res->getBody());
            if (empty($formulaire)) {
                $form = new Form(0, "Votre avis nous intéresse !!!", "Description de notre formulaire", array());
                $res = $this->client->request('POST', 'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/insertForm?'.
                                    'title='.$form->getTitle().'&'.
                                    'description='.$form->getDescription()
                );
                if ($res->getStatusCode() != 200) {
                    throw new Exception('InsertForm failed');
                }
            }
        } catch (GuzzleException $g) {
            echo "Error : " . $g->getMessage();
            throw new Exception($g->getMessage(), $g->getCode(), $g);
        }
    }


    /**
     * Permet d'ajouter une nouvelle catégorie (mot-clef)
     *
     * @return void
     * @throws Exception
     */
    public function addKeyword(): void
    {
        $keyword = Clean::simpleString($_POST['keyword']);
        try {
            if (!validate::keyword($keyword)) {
                throw new Exception('Mot-clef invalide');
            }
            $res = $this->client->request('POST', 'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/insertKeyword?'.
                                    'keyword='.$keyword
            );
            if ($res->getStatusCode() != 200) {
                throw new Exception('InsertKeyword failed');
            }
        } catch (GuzzleException | ClientException $g) {
            echo "Error : " . $g->getMessage();
            throw new Exception($g->getMessage(), $g->getCode(), $g);
        }
    }

    /**
     * Permet de supprimer une catégorie (mot-clef)
     *
     * @return void
     * @throws Exception
     */
    public function deleteKeyword(): void
    {
        try {
            $res = $this->client->request('DELETE', 'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/deleteKeyword?'.
                                'keyword='.$_POST["idCateg"]
            );
            if ($res->getStatusCode() != 200) {
                throw new Exception('DeleteKeyword failed');
            }
        } catch (GuzzleException | ClientException $g) {
            echo "Error : " . $g->getMessage();
            throw new Exception($g->getMessage(), $g->getCode(), $g);
        }
    }

    /**
     * Permet de récupérer toutes les catégories existantes.
     *
     * @return array
     * @throws Exception
     */
    public function getCategories(): array
    {
        $categories = [];
        try {
            $res = $this->client->request('GET', 'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/getAllKeyword');
            if ($res->getStatusCode() != 200) {
                throw new Exception('GetAllKeyword failed');
            }
            $res = json_decode($res->getBody());
            foreach ($res as $category) {
                $categories[] = $category["word"];
            }
        } catch (GuzzleException $g) {
            echo "Error : ".$g->getMessage();
            throw new Exception($g->getMessage(), $g->getCode(), $g);
        }

        return $categories;
    }


    /**
     * Permet de récupérer toutes les questions existantes.
     *
     * @return array
     * @throws Exception
     */
    public function getQuestions(): array
    {
       try {
            $res = $this->client->request('GET', 'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/existsForm');
           if ($res->getStatusCode() != 200) {
               throw new Exception('Exists failed');
           }
            if (json_decode($res->getBody())) {
                $res = $this->client->request('GET', 'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/getForm');
                if ($res->getStatusCode() != 200) {
                    throw new Exception('GetForm failed');
                }
                $idForm = json_decode($res->getBody())[0]->id;
                $res = $this->client->request('GET', 'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/getAllQuestions?'.
                                                'idForm='.$idForm
                );
                if ($res->getStatusCode() != 200) {
                    throw new Exception('GetAllQuestions failed');
                }
                $questionsArray = json_decode($res->getBody());
                return Factory::getBuiltObjects($questionsArray, "Question");
            } else {
                return array();
            }
       } catch (GuzzleException | ClientException $g) {
           echo "Error : " . $g->getMessage();
           throw new Exception($g->getMessage(), $g->getCode(), $g);
        }
    }


    /**
     * Permet de récupérer toutes les réponses existantes.
     *
     * @return array
     * @throws Exception
     */
    public function getResponsesCandidate(): array
    {
        try {
            $res = $this->client->request('GET', 'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/getAllListResponseOfCandidate');
            if ($res->getStatusCode() != 200) {
                throw new Exception('GetAllListResponsesOfCandidate failed');
            }
            $responsesCandidate = json_decode($res->getBody());
            $results = [];
            foreach ($responsesCandidate as $response) {
                $res = $this->client->request('GET', 'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/getDetailsListResponseOfCandidate?'.
                                                'id='.$response->id
                );
                if ($res->getStatusCode() != 200) {
                    throw new Exception('GetDetailsListResponsesOfCandidate failed');
                }
                $results[] =json_decode($res->getBody());
            }
            return $results;
        } catch (GuzzleException | ClientException $g) {
            echo "Error : " . $g->getMessage();
            throw new Exception($g->getMessage(), $g->getCode(), $g);
        }
    }

    /**
     * Permet de supprimer les réponses d'une personne d'un formulaire
     *
     * @return void
     * @throws Exception
     */
    public function deleteResponsesCandidate(): void
    {
        try {
            $res = $this->client->request('DELETE', 'https://codefirst.iut.uca.fr/containers/Temoignages-deploy_api_form/deleteListResponseOfCandidate?'.
                                            'id='.Clean::int($_POST["idResponseCandidate"])
            );
            if ($res->getStatusCode() != 200) {
                throw new Exception('DeleteListResponseOfCandidate failed');
            }
        } catch (GuzzleException | ClientException $g) {
            echo "Error : " . $g->getMessage();
            throw new Exception($g->getMessage(), $g->getCode(), $g);
        }
    }
}
