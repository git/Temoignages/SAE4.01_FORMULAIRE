<?php

namespace Controller;

use Exception;
use InvalidArgumentException;
use Model\ModelAdmin;
use Config\Clean;
use Config\Validate;

/**
 * Permet de controller les réponses à fournir en fonction des actions passer dans l'URL
 * par l'administrateur
 */
class ControllerAdmin
{
    /**
     * Ajoute une question grâce à son contenu et son type récupéré dans le tableau $_POST
     * Si la question n'est pas une question texte, on appelle un nouveau formulaire permettant
     * d'ajouter des réponses prédéfinies à la question.
     *
     * @return void
     * @throws Exception
     */
    public function addQuestion(): void
    {
        $type = Clean::simpleString($_POST['type']);
        if (empty($_POST['type'])) {
            throw new InvalidArgumentException('$_POST[\'type\'] is empty');
        }
        $idQuestion = (new ModelAdmin())->addQuestion();
        if (strcmp($type, "BusinessClass\TextQuestion") == 0) {
            $this->goToQuestions();
        } else {
            $categories = (new ModelAdmin())->getCategories();
            $questionContent = $_POST['question'];
            global $rep, $views;
            require_once($rep.$views['possibleResponsesForm']);
        }
    }

    /**
     * Supprime une question par son id récupéré par le tableau $_POST ainsi que les possibles réponses associées
     *
     * @return void
     * @throws Exception
     */
    public function deleteQuestion(): void
    {
        (new ModelAdmin)->deleteQuestion();
        $this->goToQuestions();
    }

    /**
     * Ajoute une possibilité de réponse à une question, on assigne également cette réponse
     * à des catégories. On propose ensuite à l'utilisateur de continuer l'ajout d'autres réponses.
     *
     * @return void
     * @throws Exception
     */
    public function addResponse(): void
    {
        if (empty($_POST['idQuestion'] || empty($_POST['question']) || empty($_POST['type']))) {
            throw new InvalidArgumentException('$_POST parameters is missing');
        }
        (new ModelAdmin())->addResponse();
        $categories = (new ModelAdmin())->getCategories();
        $idQuestion = Clean::int($_POST['idQuestion']);
        $questionContent = Clean::simpleString($_POST['question']);
        $type = Clean::simpleString($_POST['type']);
        global $rep, $views;
        require_once($rep.$views['continue']);
    }

    /**
     * Permet de supprimer une possible réponse par son id récupéré par le tableau $_POST
     *
     * @return void
     * @throws Exception
     */
    public function deleteResponse(): void
    {
        (new ModelAdmin)->deleteResponse();
        $this->goToQuestions();
    }


    /**
     * Permet de proposer à l'utiliser de continuer ou non à ajouter des possibilités de réponses à l'aide
     * de la fonction addResponse(). Sinon, il retourne à la page administration.
     *
     * @return void
     * @throws Exception
     */
    public function continueResponse(): void
    {
        $choose = Clean::simpleString($_POST['choose']);
        if (empty($_POST['choose'] || empty($_POST['idQuestion']) || empty($_POST['type']) || empty($_POST['question']))) {
            throw new InvalidArgumentException('$_POST parameters is missing');
        }
        if ($choose == "Oui") {
            $idQuestion = Clean::int($_POST['idQuestion']);
            $categories = (new ModelAdmin())->getCategories();
            $questionContent = Clean::simpleString($_POST['question']);
            $type = Clean::simpleString($_POST['type']);
            global $rep, $views;
            require_once($rep.$views['possibleResponsesForm']);
        } else {
            $this->goToQuestions();
        }
    }


    /**
     * Permet de créer un nouveau formulaire avec un titre et une description.
     *
     * @return void
     * @throws Exception
     */
    public function createForm(): void
    {
        (new ModelAdmin())->createForm();
    }


    /**
     * Permet d'ajouter une catégorie (mot-clef) à notre application
     *
     * @return void
     * @throws Exception
     */
    public function addKeyword(): void
    {
        (new ModelAdmin())->addKeyword();
        $this->goToCategories();
    }

    /**
     * Permet de supprimer un mot clef qui sera récupéré par le tableau $_POST
     *
     * @return void
     * @throws Exception
     */
    public function deleteKeyword(): void
    {
        (new ModelAdmin)->deleteKeyword();
        $this->goToCategories();
    }

    /**
     * Permet de naviguer jusqu'à la page de gestion des catégories
     *
     * @return void
     * @throws Exception
     */
    public function goToCategories(): void
    {
        $categories = (new ModelAdmin)->getCategories();
        global $rep, $views;
        require_once($rep.$views['categories']);
    }


    /**
     * Permet de naviguer jusqu'à la page de gestion des questions
     *
     * @return void
     * @throws Exception
     */
    public function goToQuestions(): void
    {
        $questions = (new ModelAdmin())->getQuestions();
        global $rep, $views;
        require_once($rep.$views['questions']);
    }

    /**
     * Permet de naviguer jusqu'à la page de gestion des réponses
     *
     * @return void
     * @throws Exception
     */
    public function goToResponses(): void
    {
        $responsesCandidate = (new ModelAdmin)->getResponsesCandidate();
        global $rep, $views;
        require_once($rep.$views['responses']);
    }

    public function goToAdministration(): void
    {
        global $rep, $views;
        require_once($rep.$views['admin']);
    }

    /**
     * @throws Exception
     */
    public function deleteResponsesCandidate(): void
    {
        (new ModelAdmin)->deleteResponsesCandidate();
        $this->goToResponses();
    }
}
