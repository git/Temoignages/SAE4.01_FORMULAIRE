<?php

namespace Controller;

use Model\ModelCandidate;
use Exception;

/**
 * Permet de controller les réponses à fournir en fonction des actions passer dans l'URL
 * par l'utilisateur
 */
class ControllerCandidate
{
    /**
     * Permet de naviguer jusqu'au formulaire.
     *
     * @return void
     * @throws Exception
     */
    public function goToForm(): void
    {
        global $rep, $views;
        $html = (new ModelCandidate())->getForm();

        require_once($rep.$views['form']);
    }

    public function goToAdminLogin(): void
    {
        global $rep, $views;
        require_once($rep.$views['adminLogin']);
    }

    /**
     * Permet de finaliser la saisie du formulaire et de le soumettre.
     *
     * @return void
     * @throws Exception
     */
    public function submitForm(): void
    {
        (new ModelCandidate())->submitForm();
        $this->goToThanks();
    }

    public function goToThanks(): void
    {
        global $rep, $views;
        require_once($rep.$views['thanks']);
    }

    public function login(): void
    {
        global $rep,$views;
        try {
            $model= new ModelCandidate();
            $model->login();
            if ($_SESSION['role'] == "Admin") {
                require_once($rep . $views['admin']);
            } else {
                require_once($rep . $views['adminLogin']);
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            require_once($rep . $views['adminLogin']);
        }
    }
}
