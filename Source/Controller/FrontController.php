<?php

namespace Controller;

use Exception;
use Config\Clean;
use Config\AltoRouter;

/**
 * Permet de gérer l'appel des controllers en fonction de l'action et du rôle de l'utilisateur
 */

class FrontController
{
    private AltoRouter $router;
    private array $rights;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->router = new AltoRouter();
        $this->router->setBasePath($_SERVER['BASE_URI']);
        $this->mapRoutes();
        $this->rights = array (
            'Candidate' => array('ControllerCandidate'),
            'Admin' => array('ControllerCandidate','ControllerAdmin')
        );
    }

    /**
     * @return array
     */
    public function getRights(): array
    {
        return $this->rights;
    }

    /**
     * @return AltoRouter
     */
    public function getRouter(): AltoRouter
    {
        return $this->router;
    }
    public function run(): void
    {
        global $error,$rep,$views;
        $exists=false;
        $match = $this->router->match();
        if ($match) {
            $target = $match['target'];
            $params = $match['params'];
            if (!isset($_SESSION['role']) || ($_SESSION['role']!="Admin" && $_SESSION['role']!="Candidate")) {
                $_SESSION['role'] = 'Candidate';
            }
            $role = Clean::simpleString($_SESSION['role']);

            foreach ($this->rights[$role] as $controllerName) {

                if (strcmp($controllerName, $target[0])===0) {
                    $controllerClass = '\Controller\\' . $target[0];
                    $controller = new $controllerClass();
                    $controller->{$target[1]}($params);
                    $exists=true;
                }
            }
            if (!$exists) {
                $error = '403';
                require_once($rep . $views['error']);
            }
        } else {
            // no route was matched
            $error = '404';
            require_once($rep . $views['error']);
        }
    }

    /**
     * @throws Exception
     */
    protected function mapRoutes(): void
    {
        global $controller;
        $this->router->map('GET', '/', array($controller['Candidate'], 'goToForm'), 'goToForm');
        $this->router->map('POST', '/submitForm', array($controller['Candidate'], 'submitForm'), 'submitForm');
        $this->router->map('POST', '/addQuestion', array($controller['Admin'], 'addQuestion'), 'addQuestion');
        $this->router->map('POST', '/addResponse', array($controller['Admin'], 'addResponse'), 'addResponse');
        $this->router->map('POST', '/continueResponse', array($controller['Admin'], 'continueResponse'), 'continueResponse');
        $this->router->map('POST', '/createForm', array($controller['Admin'], 'createForm'), 'createForm');
        $this->router->map('POST', '/addKeyword', array($controller['Admin'], 'addKeyword'), 'addKeyword');
        $this->router->map('GET', '/goToAdmin', array($controller['Admin'], 'goToAdmin'), 'goToAdmin');
        $this->router->map('GET', '/goToAdminLogin', array($controller['Candidate'], 'goToAdminLogin'), 'goToLogin');
        $this->router->map('POST', '/login', array($controller['Candidate'], 'login'), 'login');
        $this->router->map('GET', '/logout', array($controller['Admin'], 'logout'), 'logout');
        $this->router->map('GET', '/goToCategories', array($controller['Admin'], 'goToCategories'), 'goToCategories');
        $this->router->map('GET', '/goToQuestions', array($controller['Admin'], 'goToQuestions'), 'goToQuestions');
        $this->router->map('GET', '/goToResponses', array($controller['Admin'], 'goToResponses'), 'goToResponses');
        $this->router->map('POST', '/deleteQuestion', array($controller['Admin'], 'deleteQuestion'), 'deleteQuestion');
        $this->router->map('POST', '/deleteResponse', array($controller['Admin'], 'deleteResponse'), 'deleteResponse');
        $this->router->map('POST', '/deleteKeyword', array($controller['Admin'], 'deleteKeyword'), 'deleteKeyword');
        $this->router->map('POST', '/deleteResponsesCandidate', array($controller['Admin'], 'deleteResponsesCandidate'), 'deleteResponsesCandidate');
    }
}
