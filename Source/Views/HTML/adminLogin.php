<!DOCTYPE html>
<html lang="fr">
<head>
    <?php
        global $googleApis, $googleStatic, $poppins, $icon, $logoUCA;
    ?>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.grid-min.css">
    <link rel="stylesheet" href="Views/CSS/adminLogin.css">
    <link rel="preconnect" href="<?php echo $googleApis; ?>">
    <link rel="preconnect" href="<?php echo $googleStatic; ?>" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Barlow&display=swap" rel="stylesheet">
    <title>Admin Login Page</title>
</head>
<body class="acolorba d-flex flex-column align-items-center">
    <img src="Views/IMAGES/logoUca.png" class="logo" alt="logo" height="auto" width="auto">
    <article class="card">
        <main id="main-content">
            <section id="content">
                <form method="post" action="login" class="d-flex flex-column align-items-center">
                    <h3>Service d'authentification</h3>
                    <div id="adminRegistrationErrorPanel" class="margin-space">
                        <?php
                            if (isset($error)) {
                                echo $error;
                            }
                        ?>
                    </div>
                    <label class="margin-space" for="login">
                        <input type="text" name="login" id="login" placeholder="Identifiant" required>
                    </label>
                    <label class="margin-space" for="password">
                        <input type="password" name="password" id="password" placeholder="Mot de passe" required>
                    </label>
                    <button class="margin-space" name="submitBtn" type="submit">Se connecter</button>
                </form>
            </section>
        </main>
    </article>
</body>
</html>
