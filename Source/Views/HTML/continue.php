<!DOCTYPE html>
<html lang="fr">
<head>
    <?php
    global $googleApis, $googleStatic, $poppins, $icon, $logoUCA;
    global $jQueryMin, $bundle4, $bundle5;
    ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link type="text/css" href="Views/CSS/styles.css" rel="stylesheet" />
    <link type="text/css" href="Views/CSS/stylesForm.css" rel="stylesheet" />
    <link href="<?php echo $poppins; ?>" rel="stylesheet">
    <link rel="shortcut icon" href="<?php echo $icon; ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo $icon; ?>" type="image/x-icon">
    <link rel="preconnect" href="<?php echo $googleApis; ?>">
    <link rel="preconnect" href="<?php echo $googleStatic; ?>" crossorigin>
    <title>Formulaire de témoignage</title>
</head>
<body>

    <header class="py-1">
        <div class="container px-4 px-lg-5 my-5">
            <div class="text-center text-white">
                <h1 class="display-4 fw-bolder">Les Témoignages</h1>
                <p class="lead fw-normal text-white-50 mb-0">IUT Informatique de Clermont-Ferrand</p>
            </div>
        </div>
    </header>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Administration</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?action=GoToAddTestimony">Témoignages</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="text-center">
        <h2>Continuer d'ajouter des possibilités de réponses ?</h2>
        <form method="post" action="continueResponse">
            <input name="idQuestion" type="hidden" value="<?php /** @var int $idQuestion */
                                                            echo $idQuestion; ?>">
            <input name="question" type="hidden" value="<?php /** @var string $questionContent */
                                                            echo $questionContent; ?>">
            <input name="type" type="hidden" value="<?php /** @var string $type */
                                                            echo $type; ?>">
            <br>
            <input type="submit" name="choose" value="Oui" style="width: 20%">
            <br>
            <input type="submit" name="choose" value="Non"  style="width: 20%">
            <input type="hidden" name="action" value="continueResponse">
        </form>
    </div>

    <script src="<?php echo $jQueryMin; ?>"></script>
    <script src="<?php echo $bundle4; ?>"></script>
    <script src="<?php echo $bundle5; ?>"></script>
    <script src="Views/JS/scripts.js"></script>

</body>

</html>
