<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    global $googleApis, $googleStatic, $poppins, $icon, $logoUCA;
    ?>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="Views/CSS/base.css" />
    <link rel="preconnect" href="<?php echo $googleApis; ?>">
    <link rel="preconnect" href="<?php echo $googleStatic; ?>" crossorigin>
    <link href="<?php echo $poppins; ?>" rel="stylesheet">
    <title>Formulaire de témoignage</title>
    <link rel="shortcut icon" href="<?php echo $icon; ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo $icon; ?>" type="image/x-icon">
</head>

<body>

<img id="logoUCA" src="<?php echo $logoUCA; ?>" height="35px" width="auto" alt="logo UCA">
<h1>Administration</h1>

<div class="form-center">
    <a href="goToCategories">Les catégories</a>
    <a href="goToQuestions">Les questions</a>
    <a href="goToResponses">Les réponses</a>
</div>

<br>

<div class="form-center">
    <h3>Les catégories :</h3>
    <form method="post" action="addKeyword">
        <label for="keyword"></label><input id="keyword" name="keyword" type="text" size="25" placeholder="...">
        <input type="submit" value="Ajouter">
        <input type="hidden" name="action" value="addKeyword">
    </form>
    <br>
    <ul class="form-center">
        <form method="post" action="deleteKeyword">
            <?php
            /** @var array $categories */
            foreach ($categories as $category) {
                ?> <li><?php
                echo '<form method="post" action="deleteKeyword">';
                echo $category;
                echo '    <input type="submit" value="Delete">';
                echo '    <input type="hidden" name="idCateg" value="'.$category.'">';
                echo '    <input type="hidden" name="page" value="deleteQuestion">';
                echo '</form>';
            }
            ?>
            </li>
    </ul>
</div>

</body>

</html>
