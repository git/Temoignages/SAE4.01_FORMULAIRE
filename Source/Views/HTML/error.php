<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link type="text/css" rel="stylesheet" href="Views/CSS/common.css">
    <link type="text/css" rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <title>Error Page</title>
<body class="d-flex flex-column align-items-center">
    <h1>
    <?php if (empty($error)) {
            echo "Erreur";
        } else {
            echo $error;
        }
    ?>
    </h1>
</body>
</html>
