<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    global $googleApis, $googleStatic, $poppins, $icon, $logoUCA;
    ?>
    <meta charset="UTF-8">
    <link type="text/css" rel="stylesheet" href="Views/CSS/base.css" />
    <link rel="preconnect" href="<?php echo $googleApis; ?>">
    <link rel="preconnect" href="<?php echo $googleStatic; ?>" crossorigin>
    <link href="<?php echo $poppins; ?>" rel="stylesheet">
    <title>Formulaire de témoignage</title>
    <link rel="shortcut icon" href="<?php echo $icon; ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo $icon; ?>" type="image/x-icon">
</head>

<body>

<img id="logoUCA" src="<?php echo $logoUCA; ?>" height="35px" width="auto" alt="logo UCA">
<h1>Administration</h1>

<div class="form-center">
    <a href="goToCategories">Les catégories</a>
    <a href="goToQuestions">Les questions</a>
    <a href="goToResponses">Les réponses</a>
</div>

<br>

<div class="form-center">
    <h3>Les réponses :</h3>
    <br>
    <div id="listResponses">
        <form method="post" action="deleteResponsesCandidate">
            <?php
            /** @var array $responsesCandidate */
            foreach ($responsesCandidate as $response) { ?>
                <i><?php echo $response[0]["date"];?></i>
                <p>Catégories associées :
                    <?php
                    echo " | ";
                    foreach ($response[2] as $category)
                        if(!empty($category)) {
                            echo $category[0] . " | ";
                        }
                    ?>
                </p>
                <?php foreach ($response[1] as $questionResponses) { ?>
                    <p><i>Question : </i><?php echo $questionResponses["content"]; ?></p>
                    <p><i>Réponse : </i><?php echo $questionResponses["questionContent"]; ?></p>
                    <?php
                } ?>
                <input type="submit" value="Delete">
                <input type="hidden" name="idResponseCandidate" value="<?php echo $response[0]["id"] ?>">
                <input type="hidden" name="action" value="deleteResponsesCandidate">

                <hr><br>
                <?php
            }
            ?>
        </form>
    </div>
</div>

</body>

</html>
