<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    global $googleApis, $googleStatic, $poppins, $icon, $logoUCA;
    ?>
    <meta charset="UTF-8">
    <link type="text/css" rel="stylesheet" href="Views/CSS/base.css" />
    <link rel="preconnect" href="<?php echo $googleApis; ?>">
    <link rel="preconnect" href="<?php echo $googleStatic; ?>" crossorigin>
    <link href="<?php echo $poppins; ?>" rel="stylesheet">
    <title>Formulaire de témoignage</title>
    <link rel="shortcut icon" href="<?php echo $icon; ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo $icon; ?>" type="image/x-icon">
</head>

<body>

<img id="logoUCA" src="<?php echo $logoUCA; ?>" height="35px" width="auto" alt="logo UCA">
<h1>Administration</h1>

<div class="form-center">
    <a href="goToCategories">Les catégories</a>
    <a href="goToQuestions">Les questions</a>
    <a href="goToResponses">Les réponses</a>
</div>

<br>

<div class="form-center">
    <h3>Les questions :</h3>
    <br>
    <form method="post" action="addQuestion">
        <div>
            <label for="question">Écrivez la question : </label>
            <br>
            <input id="question" name="question" type="text" size="70">
        </div>
        <div>
            <br>

            <label for="type">Sélectionnez le type de question souhaitée :
                <br>- Text permet d'écrire la réponse librement.
                <br>- ListBox permet de choisir une réponse parmi plusieurs possibilités.
                <br>- CheckBox permet de choisir une ou plusieurs réponses parmi plusieurs possibilités.
            </label>
            <br>
            <select id="type" name="type">
                <option value="BusinessClass\TextQuestion">Text</option>
                <option value="BusinessClass\ListBoxQuestion">ListBox</option>
                <option value="BusinessClass\CheckBoxQuestion">CheckBox</option>
            </select>
        </div>

        <br>
        <input type="submit" value="Ajouter">
        <input type="hidden" name="action" value="addQuestion">
    </form>
    <br>
    <hr>
    <br>
    <ul class="form-center">
        <?php
        /** @var array $questions */
        foreach ($questions as $question) {
            ?>
            <form method="post" action="deleteQuestion">
                <li><?php echo $question->printStrategy();
                    echo '<input type="submit" value="Delete">';
                    echo '<input type="hidden" name="idQuestion" value="'.$question->getId().'">';
                    echo '<input type="hidden" name="type" value="<'.get_class($question).'">';
                    echo '<input type="hidden" name="page" value="deleteQuestion">';
               echo ' </li>';
            echo '</form>';
        }
        ?>
    </ul>
</div>

</body>

</html>
