<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    global $googleApis, $googleStatic, $poppins, $icon, $logoUCA;
    global $awesomeFont, $bootstrapMin, $bootstrapIcon, $jQueryMin, $bundle4, $bundle5;
    ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link href="<?php echo $bootstrapIcon; ?>" rel="stylesheet" />
    <link href="<?php echo $bootstrapMin; ?>" rel="stylesheet" />
    <link href="<?php echo $awesomeFont; ?>" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="Views/CSS/styles.css" />
    <link rel="stylesheet" type="text/css" href="Views/CSS/stylesForm.css" />
    <link href="<?php echo $poppins; ?>" rel="stylesheet">
    <link rel="shortcut icon" href="<?php echo $icon; ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo $icon; ?>" type="image/x-icon">
    <link rel="preconnect" href="<?php echo $googleApis; ?>">
    <link rel="preconnect" href="<?php echo $googleStatic; ?>" crossorigin>
    <title>Formulaire de témoignage</title>
</head>

<body>

    <header class="py-1">
        <div class="container px-4 px-lg-5 my-5">
            <div class="text-center text-white">
                <h1 class="display-4 fw-bolder">Les Témoignages</h1>
                <p class="lead fw-normal text-white-50 mb-0">IUT Informatique de Clermont-Ferrand</p>
            </div>
        </div>
    </header>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Administration</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="/index.php?action=GoToAddTestimony">Témoignages</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container mt-5">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-md-8">
                <form id="regForm" method="post" action="addQuestion">
                    <h1 id="register">Ajout d'une question</h1>
                    <div class="all-steps" id="all-steps">
                        <span class="step"><i class="fa"></i></span>
                        <span class="step"><i class="fa"></i></span>
                    </div>

                    <div class="tab">
                        <h6>Écrivez la question : </h6>
                        <p>
                            <label>
                                <input placeholder="Question..." oninput="this.className = ''" name="question">
                            </label>
                        </p>
                    </div>

                    <div class="tab">
                        <h6>Séléctionnez le type de question souhaitée :
                            <br>- Text permet d'écrire la réponse librement.
                            <br>- ListBox permet de choisir une réponse parmi plusieurs possibilités.
                            <br>- CheckBox permet de choisir une ou plusieurs réponses parmi plusieurs possibilités.</h6>
                        <label>
                            <select  name="type">
                                <p> <option value="BusinessClass\TextQuestion">Text</option> </p>
                                <p> <option value="BusinessClass\ListBoxQuestion">ListBox</option> </p>
                                <p> <option value="BusinessClass\CheckBoxQuestion">CheckBox</option> </p>
                            </select>
                        </label>
                    </div>

                    <div class="thanks-message text-center" id="text-message"> <img src="https://i.imgur.com/O18mJ1K.png" width="100" height="100" class="mb-4" alt="">
                        <h3>Souhaitez-vous ajouter votre question ?</h3>
                        <input type='submit' value='Ajouter' id='button'>
                        <input type='hidden' name='action' value='addQuestion'>
                    </div>
                    <div style="overflow:auto;" id="nextprevious">
                        <div style="float:right;">
                            <button type="button" id="prevBtn" onclick="nextPrev(-1)"><i class="fa fa-angle-double-left"></i></button>
                            <button type="button" id="nextBtn" onclick="nextPrev(1)"><i class="fa fa-angle-double-right"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br><br>


    <script src="<?php echo $jQueryMin; ?>"></script>
    <script src="<?php echo $bundle4; ?>"></script>
    <script src="<?php echo $bundle5; ?>"></script>
    <script src="Views/JS/scripts.js"></script>

</body>

</html>
