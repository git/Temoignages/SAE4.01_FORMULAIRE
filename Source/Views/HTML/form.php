<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    global $googleApis, $googleStatic, $poppins, $icon, $logoUCA;
    global $jQueryMin, $bundle4, $bundle5;
    ?>
    <meta charset="UTF-8">
    <link type="text/css" rel="stylesheet" href="Views/CSS/styles.css" />
    <link type="text/css" rel="stylesheet" href="Views/CSS/stylesForm.css" />
    <link rel="preconnect" href="<?php echo $googleApis; ?>">
    <link rel="preconnect" href="<?php echo $googleStatic; ?>" crossorigin>
    <link href="<?php echo $poppins; ?>" rel="stylesheet">
    <link type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
    <link type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" />
    <link type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <title>Formulaire de témoignage</title>
    <link rel="shortcut icon" href="<?php echo $icon; ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo $icon; ?>" type="image/x-icon">
</head>

<body>
    <header class="py-1">
        <div class="container px-4 px-lg-5 my-5">
            <div class="text-center text-white">
                <h1 class="display-4 fw-bolder">Les Témoignages</h1>
                <p class="lead fw-normal text-white-50 mb-0">IUT Informatique de Clermont-Ferrand</p>
            </div>
        </div>
    </header>

    <?php /** @var string $html */
    echo $html;
    ?>

    <br><br>

    <script src="<?php echo $jQueryMin; ?>"></script>
    <script src="<?php echo $bundle4; ?>"></script>
    <script src="<?php echo $bundle5; ?>"></script>
    <script type="text/javascript" src="Views/JS/scripts.js"></script>
    <script type="text/javascript" src="Views/JS/getData-Ids.js"></script>

</body>

</html>
