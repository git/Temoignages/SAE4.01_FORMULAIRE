const printCategoryButton = document.querySelector('#printCategory');
printCategoryButton.addEventListener('click', printCategories);

const printQuestionButton = document.querySelector('#printQuestion');
printQuestionButton.addEventListener('click', printQuestion);

const printFormQuestionButton = document.querySelector('#addNewQuestion');
printFormQuestionButton.addEventListener('click', addQuestion);