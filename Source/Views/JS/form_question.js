/**
 * Permet d'afficher ou non la liste des questions en fonction
 * du choix de l'utilisateur. (lorsqu'il clique sur le bouton)
 */
function printQuestion() {
    const printQuestionButton = document.querySelector('#printQuestion');
    const ul = document.querySelector("#listQuestions");

    if(printQuestionButton.innerText === "Les questions ▲") {
        printQuestionButton.innerText = "Les questions ▼";
        ul.style.visibility = "hidden";
    } else {
        printQuestionButton.innerText = "Les questions ▲";
        ul.style.visibility = "visible";
    }
}


/**
 * Permet d'afficher le formulaire permettant d'ajouter une question
 * si l'utilisateur clique sur le bouton.
 */
function addQuestion() {
    const printAddQuestionFormButton = document.querySelector('#addNewQuestion');
    const form = document.querySelector('#addQuestionForm');

    if(printAddQuestionFormButton.innerText === "Ajouter une question ▲") {
        printAddQuestionFormButton.innerText = "Ajouter une question ▼";
        form.style.visibility = "hidden";
    } else {
        printAddQuestionFormButton.innerText = "Ajouter une question ▲";
        form.style.visibility = "visible";
    }
}