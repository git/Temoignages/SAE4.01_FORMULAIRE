/**
 * Permet d'afficher la liste des catégories en fonction
 * du choix de l'utilisateur (lorsqu'il clique sur le bouton)
 */
function printCategories() {
    const printCategoryButton = document.querySelector('#printCategory');
    const ul = document.querySelector("#listCategories");

    if(printCategoryButton.innerText === "Les catégories ▲") {
        printCategoryButton.innerText = "Les catégories ▼";
        ul.style.visibility = "hidden";
    } else {
        printCategoryButton.innerText = "Les catégories ▲";
        ul.style.visibility = "visible";
    }
}
