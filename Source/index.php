<?php

use Controller\FrontController;
use Config\Autoload;

require_once(__DIR__.'/Config/Autoload.php');
require_once(__DIR__.'/Config/config.php');
require_once(__DIR__.'/Config/Autoload.php');
Autoload::charger();

session_start();

$frontController = new FrontController();
$frontController->run();
