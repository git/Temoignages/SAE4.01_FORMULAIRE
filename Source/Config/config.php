<?php

$rep = __DIR__ . '/../';

$views['form'] = 'Views/HTML/form.php';
$views['admin'] = 'Views/HTML/admin.php';
$views['adminLogin'] = 'Views/HTML/adminLogin.php';
$views['possibleResponsesForm'] = 'Views/HTML/possibleResponsesForm.php';
$views['continue'] = 'Views/HTML/continue.php';
$views['categories'] = 'Views/HTML/categories.php';
$views['questions'] = 'Views/HTML/questions.php';
$views['responses'] = 'Views/HTML/responses.php';
$views['thanks'] = 'Views/HTML/thanks.php';
$views['error'] = 'Views/HTML/error.php';

$_SERVER['BASE_URI'] = '';

$controller['Candidate'] = 'ControllerCandidate';
$controller['Admin'] = 'ControllerAdmin';

$googleApis = "https://fonts.googleapis.com";
$googleStatic = "https://fonts.gstatic.com";
$poppins = "https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap";
$icon = "https://cdn.uca.fr/images/favicon/favicon.ico";
$logoUCA = "https://cdn.uca.fr/images/logos/logo_uca_mini_light.png";
$bootstrapIcon = "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css";
$bootstrapMin = "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css";
$awesomeFont = "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css";
$jQueryMin = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js";
$bundle4 = "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js";
$bundle5 = "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js";

$GLOBALS['emailMaxLength']=150;
$GLOBALS['pseudoMaxLength']=50;
$GLOBALS['passwordMaxLength']=500;
$GLOBALS['keyWordMaxLength']=50;
$GLOBALS['titleMaxLength']=50;
$GLOBALS['typeMaxLength']=50;
$GLOBALS['responseMaxLength']=200;
$GLOBALS['usernameMaxLength']=50;
$GLOBALS["categoriesMaxLength"]=50;

