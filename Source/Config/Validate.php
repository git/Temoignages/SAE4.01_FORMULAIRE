<?php

namespace Config;

require_once "config.php";

class Validate
{
    /**
     * Valide une adresse e-mail en utilisant la fonction filter_var() de PHP et une
     * longueur maximale définie globalement.
     *
     * @param string $email L'adresse e-mail à valider.
     * @return bool Vrai si l'adresse e-mail est valide et respecte la longueur maximale définie, faux sinon.
     */

    public static function email(String $email): bool
    {
        // Vérifier si l'adresse e-mail est valide
        return filter_var($email, FILTER_VALIDATE_EMAIL) && strlen($email) <= $GLOBALS['emailMaxLength'];
    }

    /**
     * Valide un pseudo en vérifiant que la longueur est suffisante, qu'il contient uniquement des
     * caractères alphanumériques, et qu'il respecte la longueur maximale définie globalement.
     *
     * @param string $login
     * @return bool Vrai si le pseudo est valide, faux sinon.
     */

    public static function login(string $login) : bool
    {
        // Vérifier si la longueur est supérieure à 3.
        if (strlen($login) <= 3 || !ctype_alnum($login) || strlen($login) >= $GLOBALS['pseudoMaxLength']) {
            return false;
        }
        return true;
    }

    /**
     * Valide un mot de passe en vérifiant que la longueur est suffisante, qu'il contient au moins un chiffre
     * et une lettre, et qu'il respecte la longueur maximale définie globalement.
     *
     * @param string $password Le mot de passe à valider.
     * @return bool Vrai si le mot de passe est valide, faux sinon.
     */

    public static function password(string $password) : bool
    {
        return (strlen($password) >= 8 && strlen($password) <=$GLOBALS['passwordMaxLength'] &&
            preg_match("/\d/", $password) && preg_match("#[a-zA-Z]+#", $password));
    }

    /**
     * Vérifie si le mot-clé est valide.
     *
     * @param string $keyword Le mot-clé à vérifier
     * @return bool Vrai si le mot-clé est valide, faux sinon.
     */

    public static function keyWord(string $keyword) : bool
    {
        return (strlen($keyword) <= $GLOBALS['keyWordMaxLength'] && strlen($keyword) >= 3);
    }

    /**
     * Vérifie si le titre est valide.
     *
     * @param string $title Le titre à vérifier
     * @return bool Vrai si le titre est valide, faux sinon.
     */

    public static function title(string $title) : bool
    {
        return (strlen($title) <= $GLOBALS['titleMaxLength'] && strlen($title) >= 3);
    }

    /**
     * Vérifie si le type est valide.
     *
     * @param string $type Le type à vérifier
     * @return bool Vrai si le type est valide, faux sinon.
     */

    public static function type(string $type) : bool
    {
        return (strlen($type) <= $GLOBALS['typeMaxLength'] && strlen($type) >=3);
    }

    /**
     * Vérifie si la réponse est valide.
     *
     * @param string $response La réponse à vérifier
     * @return bool Vrai si la réponse est valide, faux sinon.
     */

    public static function response(string $response) : bool
    {
        return (strlen($response) <= $GLOBALS['responseMaxLength'] && !empty($response));
    }

    /**
     * Vérifie si le nom est valide.
     *
     * @param string $username
     * @return bool Vrai si le nom est valide, faux sinon.
     */

    public static function username(string $username): bool
    {
        if (strlen($username) <= 3 || !ctype_alnum($username) || strlen($username) >= $GLOBALS['usernameMaxLength']) {
            return false;
        }
        return true;
    }

    /**
     * Vérifie si la description est valide.
     *
     * @param array $categories
     * @return bool Vrai si la description est valide, faux sinon.
     */

     public static function categories(array $categories): bool
    {
        foreach ($categories as $category) {
            if (strlen($category) > $GLOBALS['categoriesMaxLength']) {
                return false;
            }
        }
        return true;
    }

}
