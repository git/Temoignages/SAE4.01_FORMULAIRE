<?php

namespace Config;

use RuntimeException;

require_once(__DIR__.'/vendor/autoload.php');

class Autoload
{
    private static mixed $instance = null;

    /**
     * @return mixed
     */
    public static function getInstance(): mixed
    {
        return self::$instance;
    }

    public static function charger(): void
    {
        if (null !== self::$instance) {
            throw new RuntimeException(sprintf('%s is already started', __CLASS__));
        }

        self::$instance = new self();


        if (!spl_autoload_register(array(self::$instance, 'autoloader'))) {
            throw new RuntimeException(sprintf('%s : Could not start the autoload', __CLASS__));
        }
    }

    public static function shutDown(): void
    {
        if (null !== self::$instance) {

            if (!spl_autoload_unregister(array(self::$instance, 'autoloader'))) {
                throw new RuntimeException('Could not stop the autoload');
            }

            self::$instance = null;
        }
    }

    private static function autoloader($className): void
    {
        $folder = "./";
        $className = ltrim($className, '\\');
        $fileName  = '';
        $namespace = '';
        if ($lastNsPos = strripos($className, '\\')) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }
        $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

        require_once $folder.$fileName;
    }
}
