<?php

namespace Config;

class Clean
{
    /**
     * Cette fonction prend une chaîne de caractères en entrée et retourne une version nettoyée de cette chaîne.
     * Elle supprime les espaces de début et de fin, ainsi que toutes les balises HTML et encode les
     * caractères spéciaux.
     *
     * @param string $string La chaîne à nettoyer
     * @return string La chaîne nettoyée
     */

    public static function simpleString(string $string): string
    {
        $string = strip_tags($string);
        $string = trim($string);
        return htmlspecialchars($string);
    }

    /**
     * Cette fonction prend un tableau de chaînes de caractères en entrée et retourne un tableau de chaînes
     * nettoyées.
     * Elle supprime les espaces de début et de fin, ainsi que toutes les balises HTML, et encode les
     * caractères spéciaux.
     *
     * @param array $array Le tableau de chaînes à nettoyer
     * @return array Le tableau de chaînes nettoyées
     */

    public static function simpleStringArray(array $array): array
    {
        $array = array_map('htmlspecialchars', $array);
        $array = array_map('trim', $array);
        $array = array_map('strip_tags', $array);
        $array = array_map(function ($element) {
            return trim(preg_replace('/\s+/', ' ', $element));
        }, $array);
        return array_map('trim', $array);
    }


    /**
     * Cette fonction prend une chaîne de caractères en entrée et retourne une version nettoyée de cette chaîne.
     * Elle supprime les espaces de début et de fin, ainsi que toutes les balises HTML et encode les
     * caractères spéciaux.
     * @param $email
     * @return string La chaîne nettoyée
     */

    public static function email($email): string
    {
        // Vérifier si l'adresse e-mail est valide
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Supprimer les caractères incorrects
            $email = preg_replace('/[^a-zA-Z0-9.@]/', '', $email);
        }
        return filter_var($email, FILTER_SANITIZE_EMAIL);

    }

    /**
     * Cette fonction prend un nombre entier en entrée, nettoie et retourne une version formatée de l'entier.
     * Elle  applique la fonction filter_var avec le filtre FILTER_SANITIZE_NUMBER_INT.
     * @param int $int Le nombre entier à nettoyer et formater
     * @return int Le nombre entier formaté
     */

    public static function int(mixed $int): int
    {

        if (!is_int($int)) {
            $cleaned = preg_replace('/[^0-9]/', '', $int);
            return filter_var(intval($cleaned), FILTER_SANITIZE_NUMBER_INT);
        }
        return filter_var($int, FILTER_SANITIZE_NUMBER_INT);
    }


}
