<?php

namespace Exceptions;

use Exception;

class InexistantLoginException extends Exception
{
    public function __construct()
    {
        parent::__construct("Identifiant inexistant");
    }
}
