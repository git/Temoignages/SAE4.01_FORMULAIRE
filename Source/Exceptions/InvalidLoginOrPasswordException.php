<?php

namespace Exceptions;

use Exception;

class InvalidLoginOrPasswordException extends Exception
{
    public function __construct()
    {
        parent::__construct("Identifiant ou mot de passe invalide");
    }
}
