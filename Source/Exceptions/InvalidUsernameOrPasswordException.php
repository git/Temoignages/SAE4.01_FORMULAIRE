<?php

namespace Exceptions;

use Exception;

class InvalidUsernameOrPasswordException extends Exception
{
    public function __construct($message = "Nom d'utilisateur ou mot de passe invalide", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
