<?php


namespace TestException;

use Exception;
use Exceptions\InvalidUsernameOrPasswordException;
use PHPUnit\Framework\TestCase;
use Exceptions\InvalidLoginOrPasswordException;

class InvalidUsernameOrPasswordExceptionTest extends TestCase
{
    /**
     * @covers InvalidUsernameOrPasswordException::constructor
     * @covers InvalidUsernameOrPasswordException::constructor
     */
    public function testConstructor()
    {
        $exception = new InvalidUsernameOrPasswordException();
        $this->assertInstanceOf(InvalidUsernameOrPasswordException::class, $exception);
        $this->assertInstanceOf(Exception::class, $exception);
        $this->assertEquals("Nom d'utilisateur ou mot de passe invalide", $exception->getMessage());
    }
}
