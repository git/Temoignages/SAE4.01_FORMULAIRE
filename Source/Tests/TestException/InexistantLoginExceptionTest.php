<?php

namespace TestException;

use Exception;
use Exceptions\InexistantLoginException;
use PHPUnit\Framework\TestCase;

class InexistantLoginExceptionTest extends TestCase
{
    /**
     * @covers InexistantLoginException::constructor
     */
    public function testConstructor()
    {
        $exception = new InexistantLoginException();
        $this->assertInstanceOf(InexistantLoginException::class, $exception);
        $this->assertInstanceOf(Exception::class, $exception);
        $this->assertEquals("Identifiant inexistant", $exception->getMessage());
    }
}
