<?php

namespace TestException;

use Exception;
use PHPUnit\Framework\TestCase;
use Exceptions\InvalidLoginOrPasswordException;

class InvalidLoginOrPasswordExceptionTest extends TestCase
{
    /**
     * @covers InvalidLoginOrPasswordException::constructor
     */
    public function testConstructor()
    {
        $exception = new InvalidLoginOrPasswordException();
        $this->assertInstanceOf(InvalidLoginOrPasswordException::class, $exception);
        $this->assertInstanceOf(Exception::class, $exception);
        $this->assertEquals("Identifiant ou mot de passe invalide", $exception->getMessage());
    }
}
