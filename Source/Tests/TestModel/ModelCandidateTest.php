<?php

namespace TestModel;

use Exception;
use Model\ModelCandidate;
use PHPUnit\Framework\TestCase;

class ModelCandidateTest extends TestCase
{
    /**
     * @covers ModelCandidate::submitForm
     * @throws Exception
     */
    public function testSubmitForm()
    {
        $_POST['answers'] = ['1||Answer1||Category1', '2||Answer2||Category2', '3||Answer3||Category3'];
        $_POST['action'] = 'submitForm';
        $_POST['data_ids'] = json_encode([4, 5]);
        $this->expectOutputString('');
        (new ModelCandidate())->submitForm();
    }

    /**
     * @covers ModelCandidate::getForm
     * @throws Exception
     */
    public function testGetForm()
    {
        $this->assertIsString((new ModelCandidate())->getForm());
    }


}
