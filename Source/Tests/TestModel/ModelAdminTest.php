<?php

namespace TestModel;

use PHPUnit\Framework\TestCase;
use Model\ModelAdmin;
use Exception;

class ModelAdminTest extends TestCase
{
    /**
     * @covers \Model\ModelAdmin::__construct
     */
    public function testConstructor()
    {
        $model = new ModelAdmin();
        $this->assertInstanceOf(ModelAdmin::class, $model);
    }

    /**
     * @covers ModelAdmin::addQuestion
     */
    public function testAddQuestionInvalidType()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Type de question invalide");
        $_POST['type']="JeSuisUnTypeInvalideDePlusDe50CaracteresEtJeSuisTropLongPourEtreUnTypeDeQuestionValide";
        $_POST['question']="Suis-je une question valide ?";
        (new ModelAdmin())->addQuestion();
    }

    /**
     * @covers ModelAdmin::deleteQuestion
     */
    public function testDeleteQuestionInvalidType()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Type de question invalide");
        $_POST['type']="JeSuisUnTypeInvalideDePlusDe50CaracteresEtJeSuisTropLongPourEtreUnTypeDeQuestionValide";
        $_POST['idQuestion']="Suis-je une question valide ?";
        (new ModelAdmin())->deleteQuestion();
    }

    /**
     * @covers ModelAdmin::addResponse
     */
    public function  testAddResponseInvalidCategories()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Categories invalides');
        $_POST['categories']=['JeSuisUneCategorieInvalideDePlusDe50CaracteresEtJeSuisTropLonguePourEtreUneCategorieValide'];
        $_POST['response']="Suis-je une réponse valide ?";
        $_POST['idQuestion']=1;
        (new ModelAdmin())->addResponse();
    }

    /**
     * @covers ModelAdmin::addKeyword
     */
    public function  testAddKeywordInvalidKeyword()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Mot-clef invalide');
        $_POST['keyword']="JeSuisUnMotCleInvalideDePlusDe50CaracteresEtJeSuisTropLonguePourEtreUnMotCleValide";
        (new ModelAdmin())->addKeyword();
    }

    /**
     * @covers ModelAdmin::getCategories
     * @throws Exception
     */
    public function testGetCategories()
    {
        $categories = (new ModelAdmin())->getCategories();
        $this->assertIsArray($categories);
    }

    /**
     * @covers ModelAdmin::getQuestions
     * @throws Exception
     */
    public function testGetQuestions()
    {
        $questions = (new ModelAdmin())->getQuestions();
        $this->assertIsArray($questions);
    }

    /**
     * @covers ModelAdmin::getResponsesCandidate
     * @throws Exception
     */
    public function testGetResponsesCandidate()
    {
        $responses = (new ModelAdmin())->getResponsesCandidate();
        $this->assertIsArray($responses);
    }
}
