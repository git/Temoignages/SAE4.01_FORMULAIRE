<?php

namespace TestBusinessClass;

use BusinessClass\IPrintQuestionStrategy;
use PHPUnit\Framework\TestCase;

class IPrintQuestionStrategyTest extends TestCase
{
    /**
     * @covers IPrintQuestionStrategy::printStrategy
     */
    public function testPrintStrategy()
    {
        $strategy = new class implements IPrintQuestionStrategy {
            public function printStrategy(): string
            {
                return '<div>Question</div>';
            }
        };

        $this->assertEquals('<div>Question</div>', $strategy->printStrategy());
    }
}
