<?php

namespace TestBusinessClass;


use BusinessClass\BoxQuestion;
use PHPUnit\Framework\TestCase;

class BoxQuestionTest extends TestCase
{
    /**
     * @covers BoxQuestion::__construct
     */
    public function testConstructorWithFourArguments()
    {
        $args = [['response1', 'response2'], 'question', ['category1', 'category2'], 1];
        $boxQuestion = new class(4, $args) extends BoxQuestion {
            public function printStrategy(): string
            {
                return '';
            }
        };

        $this->assertEquals($args[0], $boxQuestion->getPossibleResponses());
        $this->assertEquals($args[2], $boxQuestion->getCategories());
    }

    /**
     * @covers BoxQuestion::setPossibleResponses
     */
    public function testSetPossibleResponses()
    {
        $args = [1, 'question'];

        $possibleResponses = ['response1', 'response2'];
        $boxQuestion = new class(2, $args) extends BoxQuestion {
            public function printStrategy(): string
            {
                return '';
            }
        };
        $boxQuestion->setPossibleResponses($possibleResponses);

        $this->assertEquals($possibleResponses, $boxQuestion->getPossibleResponses());
    }

    /**
     * @covers BoxQuestion::setCategories
     */
    public function testSetCategories()
    {
        $args = [1, 'question'];

        $categories = ['category1', 'category2'];
        $boxQuestion = new class(2, $args) extends BoxQuestion {
            public function printStrategy(): string
            {
                return '';
            }
        };
        $boxQuestion->setCategories($categories);

        $this->assertEquals($categories, $boxQuestion->getCategories());
    }
}
