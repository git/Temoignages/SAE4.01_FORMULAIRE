<?php

namespace TestBusinessClass;

use BusinessClass\Response;
use PHPUnit\Framework\TestCase;

class ResponseTest extends TestCase
{
    private int $id = 1;
    private string $date = "2022-03-18";
    private string $titleForm = "My Form";
    private array $questionsResponses = [
        "Question 1" => "Response 1",
        "Question 2" => "Response 2"
    ];

    /**
     * @covers Response::getId
     * @covers Response::getDate
     * @covers Response::getTitleForm
     * @covers Response::getQuestionsResponses
     *
     */
    public function testGetters()
    {
        $response = new Response($this->id, $this->date, $this->titleForm, $this->questionsResponses);

        $this->assertEquals($this->id, $response->getId());
        $this->assertEquals($this->date, $response->getDate());
        $this->assertEquals($this->titleForm, $response->getTitleForm());
        $this->assertEquals($this->questionsResponses, $response->getQuestionsResponses());
    }

    /**
     * @covers Response::setDate
     * @covers Response::setTitleForm
     * @covers Response::setQuestionsResponses
     *
     */

    public function testSetters()
    {
        $response = new Response($this->id, $this->date, $this->titleForm, $this->questionsResponses);

        $newDate = "2023-03-18";
        $response->setDate($newDate);
        $this->assertEquals($newDate, $response->getDate());

        $newTitleForm = "New Form";
        $response->setTitleForm($newTitleForm);
        $this->assertEquals($newTitleForm, $response->getTitleForm());

        $newQuestionsResponses = [
            "Question 1" => "New Response 1",
            "Question 2" => "New Response 2"
        ];
        $response->setQuestionsResponses($newQuestionsResponses);
        $this->assertEquals($newQuestionsResponses, $response->getQuestionsResponses());
    }
}


