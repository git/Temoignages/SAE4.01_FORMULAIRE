<?php

namespace TestBusinessClass;

use BusinessClass\Keyword;
use PHPUnit\Framework\TestCase;

class KeywordTest extends TestCase
{
    /**
     * @covers Keyword::construct
     */
    public function testConstructor()
    {
        $id = 1;
        $word = 'example';
        $keyword = new Keyword($id, $word);

        $this->assertEquals($id, $keyword->getId());
        $this->assertEquals($word, $keyword->getWord());
    }

    /**
     * @covers Keyword::setWord
     */
    public function testSetWord()
    {
        $id = 1;
        $word = 'example';
        $newWord = 'new example';
        $keyword = new Keyword($id, $word);
        $keyword->setWord($newWord);

        $this->assertEquals($newWord, $keyword->getWord());
    }
}

