<?php

namespace TestBusinessClass;

use BusinessClass\TextQuestion;
use PHPUnit\Framework\TestCase;

class TextQuestionTest extends TestCase
{
    /**
     * @covers TextQuestion::printStrategy
     *
     */
    public function testPrintStrategy()
    {
        $content = 'What is your name?';
        $id = 1;
        $textQuestion = new TextQuestion($id, $content);
        $expectedOutput = "<div class='tab'>
                    <h6>$content</h6>
                    <p>
                        <input data-id='$id' placeholder='...' oninput='this.className = ''''  type='text' name='answers[]'>
                    </p>
                </div>\n";
        $this->assertEquals($expectedOutput, $textQuestion->printStrategy());
    }
}


