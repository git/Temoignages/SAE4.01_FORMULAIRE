<?php

namespace TestBusinessClass;

use BusinessClass\Question;
use PHPUnit\Framework\TestCase;

class QuestionTest extends TestCase
{
    /**
     * @covers Question::constructor
     */
    public function testConstructor()
    {
        $id = 1;
        $content = 'What is your name?';
        $question = new class($id, $content) extends Question {
            public function printStrategy(): string
            {
                return '';
            }
        };

        $this->assertEquals($id, $question->getId());
        $this->assertEquals($content, $question->getContent());
    }

    /**
     * @covers Question::setContent
     */
    public function testSetContent()
    {
        $content = 'What is your age?';
        $question = new class(1, 'question') extends Question {
            public function printStrategy(): string
            {
                return '';
            }
        };
        $question->setContent($content);

        $this->assertEquals($content, $question->getContent());
    }
}

