<?php

namespace TestBusinessClass;

use BusinessClass\Form;
use PHPUnit\Framework\TestCase;

class FormTest extends TestCase
{
    /**
     * @covers Form::getTitle
     */
    public function testGetTitleReturnsCorrectValue()
    {
        $form = new Form(1, 'Titre du formulaire', 'Description du formulaire', []);
        $this->assertEquals('Titre du formulaire', $form->getTitle());
    }

    /**
     * @covers Form::setTitle
     */
    public function testSetTitleSetsCorrectValue()
    {
        $form = new Form(1, 'Titre du formulaire', 'Description du formulaire', []);
        $form->setTitle('Nouveau titre');
        $this->assertEquals('Nouveau titre', $form->getTitle());
    }

    /**
     * @covers Form::getDescription
     */
    public function testGetDescriptionReturnsCorrectValue()
    {
        $form = new Form(1, 'Titre du formulaire', 'Description du formulaire', []);
        $this->assertEquals('Description du formulaire', $form->getDescription());
    }

    /**
     * @covers Form::setDescription
     */
    public function testSetDescriptionSetsCorrectValue()
    {
        $form = new Form(1, 'Titre du formulaire', 'Description du formulaire', []);
        $form->setDescription('Nouvelle description');
        $this->assertEquals('Nouvelle description', $form->getDescription());
    }

    /**
     * @covers Form::getQuestions
     */
    public function testGetQuestionsReturnsCorrectValue()
    {
        $questions = [
            'Question 1',
            'Question 2',
            'Question 3'
        ];

        $form = new Form(1, 'Titre du formulaire', 'Description du formulaire', $questions);
        $this->assertEquals($questions, $form->getQuestions());
    }

    /**
     * @covers Form::setQuestions
     */
    public function testSetQuestionsSetsCorrectValue()
    {
        $questions = [
            'Question 1',
            'Question 2',
            'Question 3'
        ];

        $form = new Form(1, 'Titre du formulaire', 'Description du formulaire', []);
        $form->setQuestions($questions);
        $this->assertEquals($questions, $form->getQuestions());
    }

    /**
     * @covers Form::getId
     */
    public function testGetIdReturnsCorrectValue()
    {
        $form = new Form(1, 'Titre du formulaire', 'Description du formulaire', []);
        $this->assertEquals(1, $form->getId());
    }
}
