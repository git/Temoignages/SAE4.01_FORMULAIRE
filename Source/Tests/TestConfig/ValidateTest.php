<?php

namespace TestConfig;


use PHPUnit\Framework\TestCase;
use Config\Validate;

class ValidateTest extends TestCase
{
    /**
     * @covers Validate::email
     */
    public function testEmail()
    {
        $this->assertTrue(Validate::email('john.doe@example.com'));
        $this->assertFalse(Validate::email('john.doe@'));
        $this->assertFalse(Validate::email('john.doe@example.commmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm'));
    }

    /**
     * @covers Validate::login
     */
    public function testLogin()
    {
        $this->assertTrue(Validate::login('john123'));
        $this->assertFalse(Validate::login('joh'));
        $this->assertFalse(Validate::login('joh!'));
        $this->assertFalse(Validate::login('john123456789012345555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555'));
    }

    /**
     * @covers Validate::password
     */
    public function testPassword()
    {
        $this->assertTrue(Validate::password('Pa$$w0rd'));
        $this->assertFalse(Validate::password('password'));
        $this->assertFalse(Validate::password('12345678'));
        $this->assertFalse(Validate::password('pa$$word'));
        $this->assertFalse(Validate::password('P@$$worddddddddddddddddddddddddddddddddddddddddddd'));
    }

    /**
     * @covers Validate::keyWord
     */
    public function testKeyWord()
    {
        $this->assertTrue(Validate::keyWord('keyword'));
        $this->assertFalse(Validate::keyWord('ke'));
        $this->assertFalse(Validate::keyWord('keyworddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd'));
    }

    /**
     * @covers Validate::title
     */
    public function testTitle()
    {
        $this->assertTrue(Validate::title('Title'));
        $this->assertFalse(Validate::title('Ti'));
        $this->assertFalse(Validate::title('titleddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd'));
    }

    /**
     * @covers Validate::type
     */
    public function testType()
    {
        $this->assertTrue(Validate::type('Type'));
        $this->assertFalse(Validate::type('Ty'));
        $this->assertFalse(Validate::type('typeddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd'));
    }

    /**
     * @covers Validate::response
     */
    public function testResponse()
    {
        $this->assertTrue(Validate::response('Response'));
        $this->assertFalse(Validate::response(''));
        $this->assertFalse(Validate::response('responseddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd'));
    }

    /**
     * @covers Validate::username
     */
    public function testUsername()
    {
        $this->assertTrue(Validate::username('john123'));
        $this->assertFalse(Validate::username('jo'));
        $this->assertFalse(Validate::username('joh!'));
        $this->assertFalse(Validate::username('john1234567890123455555555555555555555555555555555555555555555555555555555555555555555555555555555555555555'));
    }
}
