<?php

namespace TestConfig;

use Config\Clean;
use PHPUnit\Framework\TestCase;

class CleanTest extends TestCase
{
    /**
     * @covers Clean::simpleString
     */
    public function testSimpleString()
    {
        // Test avec une chaîne de caractères qui contient des balises HTML et des espaces
        $string = '<p> Test  avec  des  espaces  ! </p>';
        $expected = 'Test  avec  des  espaces  !';
        $this->assertEquals($expected, Clean::simpleString($string));

        // Test avec une chaîne de caractères qui contient des caractères spéciaux
        $string = 'Ceci est une chaîne & avec des "caractères" spéciaux !';
        $expected = 'Ceci est une chaîne &amp; avec des &quot;caractères&quot; spéciaux !';
        $this->assertEquals($expected, Clean::simpleString($string));
    }

    /**
     * @covers Clean::simpleStringArray
     */
    public function testSimpleStringArray()
    {
        // Test avec une chaîne de caractères qui contient des balises HTML et des espaces
        $array = ['Test  ','avec ','des ','trucs  !'];
        $expected = ['Test','avec','des','trucs !'];
        $this->assertEquals($expected, Clean::simpleStringArray($array));
    }

    /**
     * @covers Clean::email
     */
    public function testEmail()
    {
        // Test avec une adresse email valide
        $email = 'john.doe@example.com';
        $expected = 'john.doe@example.com';
        $this->assertEquals($expected, Clean::email($email));

        // Test avec une adresse email invalide
        $email = 'john.doe@<??|||""__##:;>example.com';
        $this->assertEquals($expected, Clean::email($email));
    }

    /**
     * @covers Clean::int
     */
    public function testInt()
    {
        // Test avec un entier valide
        $int = '1234';
        $expected = 1234;
        $this->assertEquals($expected, Clean::int($int));

        // Test avec un entier invalide
        $int = '1234abc';
        $this->assertEquals($expected, Clean::int($int));
    }
}

