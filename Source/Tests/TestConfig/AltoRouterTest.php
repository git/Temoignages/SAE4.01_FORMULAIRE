<?php

namespace TestConfig;


use Config\AltoRouter;
use Exception;
use PHPUnit\Framework\TestCase;
use RuntimeException;

class AltoRouterTest extends TestCase
{
    protected AltoRouter $router;

    public function setUp(): void
    {
        $this->router = new AltoRouter();
    }

    /**
     * @throws Exception
     * @covers AltoRouter::addRoutes
     */

    public function testAddRoutesThrowsExceptionForInvalidInput()
    {
        $this->expectException(RuntimeException::class);
        $this->router->addRoutes('invalid input');
    }

    /**
     * @covers AltoRouter::getRoutes
     */
    public function testGetRoutesReturnsArrayOfRoutes()
    {
        $this->assertIsArray($this->router->getRoutes());
    }

    /**
     * @covers AltoRouter::setBasePath
     */
    public function testSetBasePathSetsBasePath()
    {
        $this->router->setBasePath('/test');
        $this->assertEquals('/test', $this->router->getBasePath());
    }

    /**
     * @covers AltoRouter::getMatchTypes
     */
    public function testAddMatchTypesAddsMatchTypes()
    {
        $this->router->addMatchTypes(['test' => 'regex']);
        $this->assertArrayHasKey('test', $this->router->getMatchTypes());
    }

    /**
     * @throws Exception
     * @covers AltoRouter::map
     * @covers AltoRouter::getRoutes
     */
    public function testMapAddsRouteToRoutesArray()
    {
        $this->router->map('GET', '/test', 'handler');
        $this->assertEquals([['GET', '/test', 'handler', null]], $this->router->getRoutes());
    }

    /**
     * @throws Exception
     * @covers AltoRouter::map
     * @covers AltoRouter::getNamedRoutes
     */
    public function testMapAddsNamedRouteToNamedRoutesArray()
    {
        $this->router->map('GET', '/test', 'handler', 'test');
        $this->assertEquals('/test', $this->router->getNamedRoutes()['test']);
    }

    /**
     * @throws Exception
     * @covers AltoRouter::map
     */
    public function testMapThrowsExceptionForDuplicateNamedRoutes()
    {
        $this->expectException(RuntimeException::class);
        $this->router->map('GET', '/test', 'handler', 'test');
        $this->router->map('GET', '/test2', 'handler', 'test');
    }
}
