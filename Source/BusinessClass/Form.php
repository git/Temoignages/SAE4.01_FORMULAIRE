<?php

namespace BusinessClass;

/**
 * Définit un formulaire.
 */
class Form
{
    /**
     * @var int
     */
    private int $id;

    /**
     * @var string
     */
    private string $title;

    /**
     * @var string
     */
    private string $description;

    /**
     * @var array
     */
    private array $questions; // La liste des questions dans un formulaire

    /**
     * @param int $id
     * @param string $title
     * @param string $description
     * @param array $questions
     */
    public function __construct(int $id, string $title, string $description, array $questions)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->questions = $questions;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getQuestions(): array
    {
        return $this->questions;
    }

    /**
     * @param array $questions
     */
    public function setQuestions(array $questions): void
    {
        $this->questions = $questions;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
