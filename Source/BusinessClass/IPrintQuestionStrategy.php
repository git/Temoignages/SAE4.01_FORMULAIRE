<?php

namespace BusinessClass;

/**
 * Définit l'affiche d'une question.
 */
interface IPrintQuestionStrategy
{
    /**
     * Permet de définir la manière dont la question doit s'afficher en HTML.
     *
     * @return string
     */
    public function printStrategy(): string;
}
