<?php

namespace BusinessClass;

/**
 * Définit une question avec plusieurs réponses, mais une seule possible.
 */
class ListBoxQuestion extends BoxQuestion
{
    public function __construct()
    {
        parent::__construct(func_num_args(), func_get_args());
    }


    /**
     * Permet de définir la manière dont la question doit s'afficher en HTML.
     *
     * @return string
     */
    public function printStrategy(): string
    {
        $id = $this->getId();
        $content = $this->getContent();
        $possibleResponses = $this->getPossibleResponses();
        $categories = $this->getCategories();



        $html = "<div class='tab'>
                    <h6>$content</h6>
                    <select name='answers[]'>";

        for ($i = 0; $i < count($possibleResponses); $i++) {
            $categoriesSplit = $id."||".$possibleResponses[$i]."||";
            foreach ($categories[$i] as $category) {
                $categoriesSplit.= $category."_";
            }
            $html.= "<p> <option value='$categoriesSplit'>$possibleResponses[$i]</option> </p>";
        }
        $html.= "\t\t\t\t</select>
            </div>\n";

        return $html;
    }
}
