<?php

namespace BusinessClass;

/**
 * Définit une question qui propose d'écrire du texte en guise de réponse
 */
class TextQuestion extends Question
{
    /**
     * Permet de définir la manière dont la question doit s'afficher en HTML.
     *
     * @return string
     */
    public function printStrategy(): string
    {
        $content = $this->getContent();
        $id = $this->getId();

        return "<div class='tab'>
                    <h6>$content</h6>
                    <p>
                        <input data-id='$id' placeholder='...' oninput='this.className = ''''  type='text' name='answers[]'>
                    </p>
                </div>\n";
    }
}
