<?php

namespace BusinessClass;

/**
 * Définit une question avec plusieurs réponses.
 */
abstract class BoxQuestion extends Question
{
    /**
     * @var array|mixed
     */
    private array $possibleResponses;
    /**
     * @var array|mixed
     */
    private array $categories;


    public function __construct(int $ctp, array $args)
    {
        switch ($ctp) {
            case 4:
                parent::__construct($args[3], $args[1]);
                $this->categories = $args[2];
                $this->possibleResponses = $args[0];
                break;
            case 2:
                parent::__construct($args[0], $args[1]);
                break;
            default:
                break;
        }
    }

    /**
     * Permet de définir la manière dont la question doit s'afficher en HTML.
     *
     * @return string
     */
    abstract public function printStrategy(): string;

    /**
     * @return array
     */
    public function getPossibleResponses(): array
    {
        return $this->possibleResponses;
    }

    /**
     * @param array $possibleResponses
     */
    public function setPossibleResponses(array $possibleResponses): void
    {
        $this->possibleResponses = $possibleResponses;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param array $categories
     */
    public function setCategories(array $categories): void
    {
        $this->categories = $categories;
    }
}
