<?php

namespace BusinessClass;

/**
 * Définit une question à choix multiples.
 */
class CheckBoxQuestion extends BoxQuestion
{
    public function __construct()
    {
        parent::__construct(func_num_args(), func_get_args());
    }


    /**
     * Permet de définir la manière dont la question doit s'afficher en HTML.
     *
     * @return string
     */
    public function printStrategy(): string
    {
        $id = $this->getId();
        $content = $this->getContent();
        $possibleResponses = $this->getPossibleResponses();
        $categories = $this->getCategories();

        $html = "<div class='tab'>
                    <h6>$content</h6>";

        for ($i = 0; $i < count($possibleResponses); $i++) {
            $categoriesSplit = $id."||".$possibleResponses[$i]."||";
            foreach ($categories[$i] as $category) {
                $categoriesSplit.= $category."_";
            }
            $html.= "<p><input style='-webkit-appearance: checkbox;' type='checkbox' name='answers[]' value='$categoriesSplit' />
                <label>$possibleResponses[$i]</label></p>";
        }
        $html.= "\t\t\t</div>\n";

        return $html;
    }
}
