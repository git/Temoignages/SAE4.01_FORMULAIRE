<?php

namespace BusinessClass;

/**
 * Définit une possibilité de réponse à une question.
 */
class Response
{
    /**
     * @var int
     */
    private int $id;

    /**
     * @var string
     */
    private string $date;

    /**
     * @var string
     */
    private string $titleForm;

    /**
     * @var array
     */
    private array $questionsResponses;

    /**
     * @param int $id
     * @param string $date
     * @param string $titleForm
     * @param array $questionsResponses
     */
    public function __construct(int $id, string $date, string $titleForm, array $questionsResponses)
    {
        $this->id = $id;
        $this->date = $date;
        $this->titleForm = $titleForm;
        $this->questionsResponses = $questionsResponses;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getTitleForm(): string
    {
        return $this->titleForm;
    }

    /**
     * @param string $titleForm
     */
    public function setTitleForm(string $titleForm): void
    {
        $this->titleForm = $titleForm;
    }

    /**
     * @return array
     */
    public function getQuestionsResponses(): array
    {
        return $this->questionsResponses;
    }

    /**
     * @param array $questionsResponses
     */
    public function setQuestionsResponses(array $questionsResponses): void
    {
        $this->questionsResponses = $questionsResponses;
    }
}
