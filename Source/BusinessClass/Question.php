<?php

namespace BusinessClass;

/**
 * Définit une question.
 */
abstract class Question implements IPrintQuestionStrategy
{
    /**
     * @var int
     */
    private int $id;

    /**
     * @var string
     */
    private string $content;


    /**
     * @param int $id
     * @param string $content
     */
    public function __construct(int $id, string $content)
    {
        $this->id = $id;
        $this->content = $content;
    }


    /**
     * Permet de définir la manière dont la question doit s'afficher en HTML.
     *
     * @return string
     */
    abstract public function printStrategy(): string;

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
