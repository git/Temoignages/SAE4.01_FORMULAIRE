<?php

namespace BusinessClass;

/**
 * Définit une catégorie associable à une réponse.
 */
class Keyword
{
    /**
     * @var int
     */
    private int $id;

    /**
     * @var string
     */
    private string $word;

    /**
     * @param int $id
     * @param string $word
     */
    public function __construct(int $id, string $word)
    {
        $this->id = $id;
        $this->word = $word;
    }


    /**
     * @return string
     */
    public function getWord(): string
    {
        return $this->word;
    }

    /**
     * @param string $word
     */
    public function setWord(string $word): void
    {
        $this->word = $word;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
